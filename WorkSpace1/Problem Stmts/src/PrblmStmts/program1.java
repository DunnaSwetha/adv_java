package PrblmStmts;

	import java.util.ArrayList;
	import java.util.List;

	public class program1 {

	    public static void program1(int[] nums) {
	        int n = nums.length;

	        // Calculate the expected sum and sum of squares
	        int expectedSum = n * (n + 1) / 2;
	        int expectedSumOfSquares = n * (n + 1) * (2 * n + 1) / 6;

	        // Calculate the actual sum and sum of squares
	        int actualSum = 0;
	        int actualSumOfSquares = 0;

	        for (int num : nums) {
	            actualSum += num;
	            actualSumOfSquares += num * num;
	        }

	        // Calculate the difference to find the missing and repeated numbers
	        int deltaSum = actualSum - expectedSum;
	        int deltaSumOfSquares = actualSumOfSquares - expectedSumOfSquares;

	        // Calculate the repeated number (A) and the missing number (B)
	        int repeatedNumber = (deltaSumOfSquares + deltaSum * deltaSum) / (2 * deltaSum);
	        int missingNumber = repeatedNumber - deltaSum;

	        List<Integer> result = new ArrayList<>();
	        result.add(repeatedNumber);
	        result.add(missingNumber);

}
	}
